# SFPG-BS-Reskin
Single File PHP Gallery Bootstrap 4 Reskin. Not complete. Only reskinned portions are the "guest" areas. Admin functions were not skinned and slideshow button was removed. Also removed the second "nav" that showed the breadcrumbs. Feel free to build on this. 

## Screenshots

**Before**
![Before](https://i.imgur.com/5RMEvst.png)


**After**
![After](https://i.imgur.com/FXYL9UI.png)
